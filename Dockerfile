FROM alpine:3.7

COPY ./requirements.txt /requirements.txt

RUN set -ex; \
        \
        apk update; \
        apk upgrade; \
        apk add --no-cache \
                bash docker gcc git libffi libffi-dev libressl-dev linux-headers make musl-dev openssh \
                py2-pip python2-dev qemu-img ruby ruby-dev ruby-ffi ruby-io-console ruby-json wget \
                py3-pip python3-dev unzip xz \
        ; \
        wget https://releases.hashicorp.com/vagrant/2.0.4/vagrant_2.0.4_x86_64.tar.xz && \
        tar -xf vagrant_2.0.4_x86_64.tar.xz -C / && \
        rm vagrant_2.0.4_x86_64.tar.xz && \
        ln -s /opt/vagrant/bin/vagrant /bin/vagrant && \
        vagrant plugin install vagrant-openstack-provider ; \
        wget https://releases.hashicorp.com/packer/1.2.3/packer_1.2.3_linux_amd64.zip && \
        unzip -q packer_1.2.3_linux_amd64.zip && \
        rm packer_1.2.3_linux_amd64.zip && \
        mv packer /bin/ ; \
        wget https://releases.hashicorp.com/terraform/0.12.9/terraform_0.12.9_linux_amd64.zip && \
        unzip -q terraform_0.12.9_linux_amd64.zip && \
        rm terraform_0.12.9_linux_amd64.zip &&\
        mv terraform /bin/ ; \
        pip3 install --upgrade pip && \
        pip3 install --ignore-installed --no-cache-dir -r /requirements.txt

COPY ./entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
